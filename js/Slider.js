/*
 * jQuery Slider v0.9.1
 *
 * Licensed under The MIT License
 *
 * Copyright (c) 2013 Jochen Achmüller
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

(function($) {
    "use strict";
    $.fn.Slider = function(settings) {
        //the default options
        var defaults = {
            mode: 'vertical',
            easing: 'linear',
            startPage: 0,
            duration: 500,
            loop: false,
            showPageNumbers: true,
            
            autoPlay: true,
            autoPlayDelay: 5000,
            autoPlayDirection: 1,
            autoPlayHoverPause: true,
            autoPlayHoverContinue: false,
            autoPlaySlideStop: true,
            
            swipe: true,                        // If touch swipe is enabled
            swipeMouse: true,                   // If mouse swipe is enabled - as addition
            swipeDrag: true,                    // If swipe drag is enabled (continuous drag)
            swipeMin: 200,                      // The minimum distance for no continuous swipe drag
            swipeDragMinPercentage: 0.3,        // The minimum swipe drag percentage to change page
            swipeMax: 800,                      // The maximum distance for swipe drag (100%)
            swipePreventDefault: true,          // Prevent the default event of the swipe
            swipeDirection: 'x',                // The swipe direction - X as default
            
            onStartSlide: function() {},        // Start slied event
            onEndSlide: function() {},          // End slide event
            
            customMode: {                       // Custom slide actions for a personal slider
                customModeSlideAction: function() {},
                customModeSwipeAction: function() {}
            },
            
            /*selectors: {
                pagedisplay: ".slidepage",
                activepage: "slidepage_active",
                content: ".slidercontent",
                slide: ".slideable",
                next: ".slidenext",
                back: ".slideback"
            }*/
        };
        //overwrite the defaults with the user options
        var options = $.extend(defaults, settings);
        
        //#region members
        
        //all needed variables
        var currentSlide = 0;
        var maxSlides = 0;
        var base = this;
        var isSliding = false;
        var swipePercentage = 0;
        var $parent = null;
        var $content = null;
        var $children = null;
        var $pages = null;
        var sliderWidth = 0;
        var sliderHeight = 0;
        var customActionDefined = false;
        var autoPlaying = false;
        var autoPlayTimer = null;
        var swiping = false;
        var oldpage = 0;
        var targetPage = 0;
        //swipe positions
        var swipeStart = {
            x: 0,
            y: 0
        };
        var swipePos = {
            x: 0,
            y: 0
        };
        var autoPlayActive = true;
        
        // Get Properties
        
        /*
         * Get the current slide page
         */
        this.getCurrentSlide = function() {
            return currentSlide;
        };
        
        /*
         * Get the total slide pages
         */
        this.getSlideCount = function() {
            return maxSlides;
        };
        
        /*
         * Get the Slider width
         */
        this.getSliderWidth = function() {
            if (sliderWidth === 0) {
                sliderWidth = $content.outerWidth();
            }
            return sliderWidth;
        };
        
        /*
         * Get the Slider height
         */
        this.getSliderHeight = function() {
            if (sliderHeight === 0) {
                sliderHeight = $content.outerHeight();
            }
            return sliderHeight;
        };
        
        /*
         * Slide to a Page
         */
        this.SlideTo = function(page) {
            if (!isSliding && currentSlide !== page) {
                //what page we are sliding to
                if (page > maxSlides - 1) {
                    if (options.loop) {
                        page = page % maxSlides;
                    } else {
                        return;
                    }
                }
                if (page < 0) {
                    if (options.loop) {
                        page = (page % maxSlides) + maxSlides;
                    } else {
                        return;
                    }
                }
                isSliding = true;
                //set it if it is 0
                //returns a wrong number at load
                this.getSliderHeight();
                this.getSliderWidth();
                //get the direction to slide
                var closest = 0;
                if (options.loop) {
                    closest = getClosestDirection(currentSlide, page, maxSlides);
                } else {
                    closest = Normalize(page - currentSlide);
                }
                //start slide event
                options.onStartSlide(currentSlide);
                var $p = $($children[page]);
                var $c = $($children[currentSlide]);
                switch(options.mode) {
                    case "fade": {
                        //fade mode
                        $p.hide().fadeTo(options.duration, 1, options.easing);
                        $c.show().fadeTo(options.duration, 0, options.easing, function() {
                            $(this).hide();
                        }); //fadeIn/Out does not work if the element got the opacity property
                        break;
                    }
                    case "horizontal": {
                        //horizontal slide mode
                        if (closest > 0) {
                            $p.css('left', sliderWidth).show().animate({
                                'left': 0
                            }, options.duration, options.easing);
                            $c.css('left', 0).show().animate({
                                'left': -sliderWidth
                            }, options.duration, options.easing, function() {
                                $(this).hide();
                            });
                        } else {
                            $p.css('left', -sliderWidth).show().animate({
                                'left': 0
                            }, options.duration, options.easing);
                            $c.css('left', 0).show().animate({
                                'left': sliderWidth
                            }, options.duration, options.easing, function() {
                                $(this).hide();
                            });
                        }
                        break;
                    }
                    case "vertical": {
                        //vertical slide mode
                        if (closest > 0) {
                            $p.css('top', sliderHeight).show().animate({
                                'top': 0
                            }, options.duration, options.easing);
                            $c.css('top', 0).show().animate({
                                'top': -sliderHeight
                            }, options.duration, options.easing, function() {
                                $(this).hide();
                            });
                        } else {
                            $p.css('top', -sliderHeight).show().animate({
                                'top': 0
                            }, options.duration, options.easing);
                            $c.css('top', 0).show().animate({
                                'top': sliderHeight
                            }, options.duration, options.easing, function() {
                                $(this).hide();
                            });
                        }
                        break;
                    }
                    default: {
                        //TODO: make a demo and test it
                        //your own slid mode
                        options.customMode.customModeSlideAction($p, $c, closest, options.duration, options.easing);
                        break;
                    }
                }
                //set the current page
                currentSlide = page;
                //after the slide is done
                setTimeout(function() {
                    isSliding = false;
                    options.onEndSlide(currentSlide);
                    ChangePage(currentSlide);
                }, options.duration);
            }
        };
        
        /*
         * Start swiping
         */
        function StartSwipe(e) {
            if(!isSliding) {
                //where did we started swiping?
                swipeStart = GetCursorPosition(e);
                options.onStartSlide(currentSlide);
                swiping = true;
            }
        }
        
        /*
         * Swipe drag action
         */
        function Drag(closest, page, percent, dur) {
            var $p = $($children[page]);
            var $c = $($children[currentSlide]);
            switch(options.mode) {
                case "fade": {
                    $p.css('display', 'block').animate({
                        'opacity': percent
                    }, dur, options.easing);
                    $c.css('display', 'block').animate({
                        'opacity': 1 - percent
                    }, dur, options.easing);
                    break;
                }
                case "horizontal": {
                    var scaled = percent * sliderWidth;
                    if (closest > 0) {
                        $p.show().animate({
                            'left': sliderWidth - scaled
                        }, dur, options.easing);
                        $c.show().animate({
                            'left': 0 - scaled
                        }, dur, options.easing);
                    } else {
                        $p.show().animate({
                            'left': -sliderWidth + scaled
                        }, dur, options.easing);
                        $c.show().animate({
                            'left': 0 + scaled
                        }, dur, options.easing);
                    }
                    break;
                }
                case "vertical": {
                    var scaled = percent * sliderHeight;
                    if (closest > 0) {
                        $p.show().animate({
                            'top': sliderHeight - scaled
                        }, dur, options.easing);
                        $c.show().animate({
                            'top': 0 - scaled
                        }, dur, options.easing);
                    } else {
                        $p.show().animate({
                            'top': -sliderHeight + scaled
                        }, dur, options.easing);
                        $c.show().animate({
                            'top': 0 + scaled
                        }, dur, options.easing);
                    }

                    break;
                }
                default: {
                    //TODO: custom mode support
                    options.customMode.customModeSwipeAction($p, $c, closest, dur, options.easing, percent);
                    break;
                }
            }
            if (percent === 1 && page !== currentSlide) {
                var c = currentSlide;
                setTimeout(function() {
                    $($children[c]).hide();
                }, dur);
            }
            if (percent === 0 && page !== currentSlide) {
                var p = page;
                setTimeout(function() {
                    $($children[p]).hide();
                }, dur);
            }
        }
        
        /*
         * Swipe move
         */
        function MoveSwipe(e) {
            if (options.swipePreventDefault) {
                e.preventDefault();
            }
            //only check if we are swiping and its not stoped yet
            if (swiping) {
                //how far did we swiped?
                swipePos = GetCursorPosition(e);
                
                //set it if it is 0
                //returns a wrong number at load
                if (sliderWidth === 0) {
                    sliderWidth = $content.outerWidth();
                }
                if (sliderHeight === 0) {
                    sliderHeight = $content.outerHeight();
                }
                if (options.swipeDrag) {
                    //TODO: add drag support
                    isSliding = true;
                    var dist = 0;
                    if (options.swipeDirection === 'y') {
                        dist = DistanceY(swipeStart, swipePos);
                    } else {
                        dist = DistanceX(swipeStart, swipePos);
                    }
                    var normalized = Normalize(dist);
                    var absolute = Math.min(Math.abs(dist), options.swipeMax);
                    
                    var closest = getClosestDirection(currentSlide, currentSlide + normalized, maxSlides);
                    var page = currentSlide + closest;
                    if (options.loop) {
                        page = WrapRange(page, maxSlides - 0);
                    } else {
                        if (page < 0) {
                            page = 0;
                            absolute = 0;
                        } else if (page > maxSlides - 1) {
                            page = maxSlides;
                            absolute = 0;
                        }
                        
                    }
                    
                    swipePercentage = absolute / options.swipeMax;
                    $($children[oldpage]).hide();
                    oldpage = page;
                    targetPage = page;
                    Drag(closest, page, swipePercentage, 0);
                } else {
                    var dist = 0;
                    if (options.swipeDirection === 'y') {
                        dist = DistanceY(swipeStart, swipePos);
                    } else {
                        dist = DistanceX(swipeStart, swipePos);
                    }
                    if (Math.abs(dist) > options.swipeMin) {
                        base.SlideTo(currentSlide + Normalize(dist));
                        StopSwipe();
                    }
                }
            }
        }
        
        /*
         * Abort the swipe
         */
        function StopSwipe() {
            swiping = false;
        }
        
        /*
         * Swipe ending
         */
        function EndSwipe(e) {
            if (swiping) {
                swiping = false;
                var dir = getClosestDirection(currentSlide, targetPage, maxSlides);
                var dur = 0;
                if (dir !== 0) {
                    if(swipePercentage > options.swipeDragMinPercentage) {
                        dur = options.duration * (1 - swipePercentage);
                        Drag(dir, targetPage, 1, dur);
                        currentSlide = targetPage;
                    } else {
                        dur = options.duration * swipePercentage;
                        Drag(dir, targetPage, 0, dur);
                    }
                    
                    setTimeout(function() {
                        isSliding = false;
                        options.onEndSlide(currentSlide);
                        ChangePage(currentSlide);
                    }, dur);
                } else {
                    isSliding = false;
                    options.onEndSlide(currentSlide);
                    ChangePage(currentSlide);
                }
            }
        }
        
        /*
         * Slides to the next page
         */
        this.SlideNext = function() {
            base.SlideTo(currentSlide + 1);
        };
        
        /*
         * Slides to the previous page
         */
        this.SlideBack = function() {
            base.SlideTo(currentSlide - 1);
        };
        
        /*
         * Starts the auto play
         */
        this.StartAutoPlay = function() {
            if (!autoPlaying) {
                //save the timer id to stop it afterwards
                autoPlayTimer = setInterval(AutoPlayAction, options.autoPlayDelay);
                autoPlaying = true;
            }
        };
        
        /*
         * Stops the auto play
         */
        this.StopAutoPlay = function() {
            if (autoPlaying) {
                clearInterval(autoPlayTimer);
                autoPlaying = false;
            }
        };
        
        /*
         * The auto play action
         */
        function AutoPlayAction() {
            base.SlideTo(currentSlide + options.autoPlayDirection);
        }
        
        function ChangePage(page) {
            if ($pages !== null) {
                $($pages.children(".slidepage").removeClass("slidepage_active")[page]).addClass("slidepage_active");
            }
        }
        
        /*
         * Slider init
         */
        this.initSlide = function() {
            $parent = $(this);
            $content = $parent.children(".slidercontent");
            $children = $content.children(".slideable");
            $children.bind('dragstart', function(e) {
                e.preventDefault();
            });
            currentSlide = options.startPage;
            maxSlides = $children.size();
            
            // Add swipe
            if (options.swipe) {
                $content.bind("touchstart", StartSwipe);
                $content.bind("touchmove", MoveSwipe);
                $content.bind("touchend touchcancel", EndSwipe);
                if (options.swipeMouse) {
                    $content.bind("mousedown", StartSwipe);
                    $content.bind("mousemove", MoveSwipe);
                    $(document).bind("mouseup", EndSwipe);
                }
            }
            //auto play
            if (options.autoPlay) {
                base.StartAutoPlay();
            }
            if (options.autoPlayHoverPause) {
                $content.bind("touchstart",function() {
                    base.StopAutoPlay();
                });
                $content.mouseenter(function() {
                    base.StopAutoPlay();
                });
            }
            if(options.autoPlayHoverContinue) {
                $(document).bind("touchstart",function(e) {
                    if (!autoPlaying && $(e.target).parent('.slideable').length === 0) {
                        base.StartAutoPlay();
                    }
                });
                $content.mouseleave(function() {
                    base.StartAutoPlay();
                });
            }
            
            //prepare the slider
            $children.hide();
            $($children[currentSlide]).show();
            
            //add navigation arrows events
            $parent.find(".slidenext").click(function() {
                base.SlideNext();
                base.StopAutoPlay();
            });
            $parent.find(".slideback").click(function() {
                base.SlideBack();
                base.StopAutoPlay();
            });
            
            $pages = $parent.find(".sliderpages");
            if ($pages !== null) {
                for(var i = 0; i < maxSlides; i++) {
                    var $pagescontent = $("<div class='slidepage'>" + ((options.showPageNumbers) ? (i + 1).toString() : "") + "</div>");
                    $pages.append($pagescontent);
                }
                $pages.children(".slidepage").click(function() {
                    base.SlideTo($(this).index());
                });
                
                // TODO: replace with css
                /*$(window).load(function() {
                    var $sliderpages = $parent.children('.sliderpages');
                    $sliderpages.css({
                        'margin-left': (base.getSliderWidth() - $sliderpages.outerWidth()) / 2
                    });
                });*/
            }
            ChangePage(currentSlide);
        };
        //this.each(function() {
            //only init if the element has children
            if ($(this).children(".slidercontent").children().size() > 1) {
                base.initSlide();
            }
        //});
        return this;
    };
})(jQuery);

/*
 * Helper functions
 */

// Returns a random integer between min and max
function randomRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
// Returns the shortest distance and the direction to a target (-X = left, +X = right)
function getClosestDirection(val, target, max) {
    var firstVal = 0;
    var half = max / 2;
    firstVal = target - val;
    if (firstVal > half) {
        firstVal = firstVal - max;
    } else if (firstVal < -half) {
        firstVal = firstVal + max;
    }
    return firstVal;
}
// Returns the distance between 2 points 
function Distance(a, b) {
    var dx = a.x - b.x;
    var dy = a.y - b.y;
    return Math.sqrt(dx * dx + dy * dy);
}
// Returns the x distance of 2 points
function DistanceX(a, b) {
    return a.x - b.x;
}
// Returns the y distance of 2 points
function DistanceY(a, b) {
    return a.y - b.y;
}
// Returns Sign of a value with 0 => 1
function Normalize(val) {
    if (val < 0) {
        return -1;
    } else {
        return 1;
    }
}
// Retruns a wraped value between 0 and max
function WrapRange(val, max) {
    if (val >= max) {
        val = val % max;
    } else if (val < 0) {
        val = (val % max) + max;
    }
    return val;
}
// Retruns a clamped value between 0 and max
function ClampRange(val, max) {
    if (val < 0) {
        return 0;
    } else if (val > max) {
        return max;
    }
    return val;
}
// Returns the cursor position of an event with temporal ie fix
function GetCursorPosition(e) {
    var cPos = {
        x: 0,
        y: 0
    };
    if (e.originalEvent.touches) {
        var touch = e.originalEvent.touches[0];
        cPos.x = touch.pageX;
        cPos.y = touch.pageY;
    } else {
        cPos.x = e.originalEvent.pageX;
        cPos.y = e.originalEvent.pageY;
        if (cPos.x === undefined || cPos.y === undefined) {
            cPos.x = event.clientX + (document.documentElement.scrollLeft || document.body.scrollLeft);
            cPos.y = event.clientY + (document.documentElement.scrollTop || document.body.scrollTop);
        }
    }
    return cPos;
}